#include <iostream>
#include <unistd.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

const char *vertexShaderSource = "#version 330 core\n"
	"layout (location = 0) in vec3 aPos;\n"
	"void main(){\n"
	"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
	"}\0";

const char *fragmentShaderSource = "#version 330 core\n"
	"out vec4 FragColor;\n"
	"void main(){\n"
	"    FragColor = vec4(1.0f,1.0f, 1.0f, 1.0f);\n"
	"}\0";

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}  

void processInput(GLFWwindow *window)
{
	if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
		glfwSetWindowShouldClose(window, true);
	}
}

void processColors(GLFWwindow *window, float *colorOne, float *colorTwo, float *colorThree){
	if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		*colorOne -= 0.01f;
	}
	if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		*colorOne += 0.01f;
	}
	if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		*colorTwo -= 0.01f;
	}
	if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		*colorTwo += 0.01f;
	}
	if(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS){
		*colorThree -= 0.01f;
	}
	if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		*colorThree += 0.01f;
	}
}

int main()
{
	float color = 0.01f;
	float colorTwo = 0.01f;
	float colorThree = 0.01f;
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "Learn Open GL", NULL, NULL);
	if(window == NULL){
		std::cout << "Failed to create GLFW Window" << std::endl;
		glfwTerminate();
		return -1; 
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback); 
	
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
	    std::cout << "Failed to initialize GLAD" << std::endl;
	    return -1;
	}    

	// Setting up the vertex shader,
	// which we have stored as a C string elsewhere
	unsigned int vertexShader; 
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	// Attach shader source to the shader object.
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	// checking for shader compile success
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout 
			<< "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" 
			<< infoLog 
			<< std::endl;
	}

	// fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	// shaderProgram attach shaders to it
	unsigned int shaderProgram; 
	shaderProgram = glCreateProgram(); 
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if(!success) {
	    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
	    std::cout 
			<< "ERROR::PROGRAM::LINKING_FAILED\n" 
			<< infoLog 
			<< std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// our triangle as vertex data
	float verticies[] = {
	    -0.5f, -0.5f, 0.0f,
	     0.5f, -0.5f, 0.0f,
	     0.0f,  0.5f, 0.0f
	};
	
	// Vertex Buffer Object
	// stores large number of verticies on GPU's memory
	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);

	// cpoy our verticies array in a buffer for OpenGL to use
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), verticies, GL_STATIC_DRAW);

	// set vertex attribute pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); 



	while(!glfwWindowShouldClose(window)){
		processInput(window);
		processColors(window, &color, &colorTwo, &colorThree);

		glClearColor(color, colorTwo, colorThree, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES,0,3);

	    glfwSwapBuffers(window);
	    glfwPollEvents();    
	} 
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);

	glfwTerminate();
	return 0;
}